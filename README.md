Role Name
=========

Install CERN ROOT

https://root.cern.ch

Requirements
------------

EPEL

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: desktop
      roles:
         - dietrichliko.root

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
